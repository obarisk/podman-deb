from debian:bullseye-slim

RUN apt-get update && apt install -y curl gnupg\
    && sh -c 'echo "deb http://repo.aptly.info/ squeeze main" >> /etc/apt/sources.list' \
    && sh -c 'curl -q https://www.aptly.info/pubkey.txt | apt-key add -'\
    && apt-get update \
    && apt-get install aptly -y