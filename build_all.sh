#!/bin/sh
CONMON_VERSION="2.1.0"
CNI_PLUGINS_VERSION="1.1.1"
PODMAN_VERSION="4.0.3"
RUNC_VERSION="1.1.1"
NETAVARK_VERSION="1.0.2"
AARDVARK_VERSION="1.0.2"

# docker build -t conmon-build -f ./conmon/Dockerfile .
# docker run --env=CONMON_VERSION=$CONMON_VERSION -v "$(pwd)/packages:/packages" -i conmon-build bash < conmon/build.sh

# docker build -t cni-build -f ./cni-plugins/Dockerfile .
# docker run --env=CNI_PLUGINS_VERSION=$CNI_PLUGINS_VERSION -v "$(pwd)/packages:/packages" -i cni-build bash < cni-plugins/build.sh

# docker build -t podman-build -f ./podman/Dockerfile .
# docker run --env=PODMAN_VERSION=$PODMAN_VERSION -v "$(pwd)/packages:/packages" -i podman-build bash < podman/build.sh

# docker build -t runc-build -f ./runc/Dockerfile .
# docker run --env=RUNC_VERSION=$RUNC_VERSION -v "$(pwd)/packages:/packages" -i runc-build bash < runc/build.sh

# docker build -t netavark-build -f ./netavark/Dockerfile .
# docker run --env=NETAVARK_VERSION=$NETAVARK_VERSION -v "$(pwd)/packages:/packages" -i netavark-build bash < netavark/build.sh

docker build -t aardvark-build -f ./aardvark-dns/Dockerfile .
docker run --env=AARDVARK_VERSION=$AARDVARK_VERSION -v "$(pwd)/packages:/packages" -i aardvark-build bash < aardvark-dns/build.sh